ACCT Profiles
=============
`acct` profile information is implicitly passed to the appropriate function calls via the `ctx` parameter.

.. code-block:: python

    # __func_alias__ makes sure the "list_" function is put on the hub as "list"
    # We use this for function names that clash with python builtin names
    __func_alias__ = {"list_": "list"}


    async def list_(hub, ctx):
        await hub.tool.my_cloud.request(ctx, "api/reference/to/resource/list")

ACCT Plugins
============
